﻿/*
MIT License

Copyright(c) 2018 Hammond Soares

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

using FileSurf.ViewModel;
using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Media.Imaging;

namespace FileSurf.Model
{
    /// <summary>
    /// Class that holds the information about a file.
    /// </summary>
    /// <seealso cref="FileSurf.ViewModel.BaseViewModel" />
    public class DisplayItemInfo : BaseViewModel
    {
        /// <summary>
        /// The folder icon source path.
        /// </summary>
        private const string FolderIcon = "pack://application:,,,/FileSurf;component/Assets/Open file.png";

        /// <summary>
        /// The file information.
        /// </summary>
        private FileInfo _fileInfo;

        /// <summary>
        /// The directory.
        /// </summary>
        private DirectoryInfo _directory;

        /// <summary>
        /// The file name.
        /// </summary>
        private string _filename;

        /// <summary>
        /// The directory name.
        /// </summary>
        private string _directoryName;

        /// <summary>
        /// The file size.
        /// </summary>
        private long? _fileSize;

        /// <summary>
        /// The file extension.
        /// </summary>
        private string _fileExtension;

        /// <summary>
        /// The date and time the file was created.
        /// </summary>
        private DateTime _createdOn;

        /// <summary>
        /// The date and time the file was last modified.
        /// </summary>
        private DateTime _modifiedOn;

        /// <summary>
        /// The file path.
        /// </summary>
        private string _filePath;

        /// <summary>
        /// Gets a value indicating whether this is a directory.
        /// </summary>
        public bool IsDirectory => FileSize == null;

        /// <summary>
        /// Gets the icon to be displayed.
        /// </summary>
        public BitmapImage Icon { get; }

        /// <summary>
        /// Gets or sets the file information.
        /// </summary>
        public FileInfo FileInfo
        {
            get => _fileInfo;
            set
            {
                _fileInfo = value;
                OnPropertyChanged(nameof(FileInfo));
            }
        }

        /// <summary>
        /// Gets or sets the directory.
        /// </summary>
        public DirectoryInfo Directory
        {
            get => _directory;
            set
            {
                _directory = value;
                OnPropertyChanged(nameof(Directory));
            }
        }

        /// <summary>
        /// Gets or sets the file name.
        /// </summary>
        public string Filename
        {
            get => _filename;
            set
            {
                _filename = value;
                OnPropertyChanged(nameof(Filename));
            }
        }

        /// <summary>
        /// Gets or sets the name of the directory.
        /// </summary>
        public string DirectoryName
        {
            get => _directoryName;
            set
            {
                _directoryName = value;
                OnPropertyChanged(nameof(DirectoryName));
            }
        }

        public long? FileSize
        {
            get => _fileSize;
            set
            {
                _fileSize = value;
                OnPropertyChanged(nameof(FileSize));
            }
        }

        /// <summary>
        /// Gets or sets the file extension.
        /// </summary>
        public string FileExtension
        {
            get => _fileExtension;
            set
            {
                _fileExtension = value;
                OnPropertyChanged(nameof(FileExtension));
            }
        }

        /// <summary>
        /// Gets or sets the created on.
        /// </summary>
        public DateTime CreatedOn
        {
            get => _createdOn;
            set
            {
                _createdOn = value;
                OnPropertyChanged(nameof(CreatedOn));
            }
        }

        /// <summary>
        /// Gets or sets the modified on.
        /// </summary>
        public DateTime ModifiedOn
        {
            get => _modifiedOn;
            set
            {
                _modifiedOn = value;
                OnPropertyChanged(nameof(ModifiedOn));
            }
        }

        /// <summary>
        /// Gets or sets the file path.
        /// </summary>
        public string FilePath
        {
            get => _filePath;
            set
            {
                _filePath = value;
                OnPropertyChanged(nameof(FilePath));
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DisplayItemInfo"/> class.
        /// </summary>
        /// <param name="info">The information.</param>
        public DisplayItemInfo(FileInfo info)
        {
            FileInfo = info;
            Directory = info.Directory;
            Filename = info.Name;
            CreatedOn = info.CreationTime;
            ModifiedOn = info.LastWriteTime;
            FilePath = info.FullName;
            DirectoryName = info.DirectoryName;
            FileSize = GetFileSize(info);
            FileExtension = (IsDirectory ? string.Empty : info.Extension);
            Icon = GetImage();
        }

        /// <summary>
        /// Gets the size of the file.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <returns></returns>
        private long? GetFileSize(FileInfo info)
        {
            try
            {
                return info.Length;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        /// <summary>
        /// Gets the image.
        /// </summary>
        /// <returns>The icon image to be displayed.</returns>
        private BitmapImage GetImage()
        {
            BitmapImage image = null;
            try
            {
                if (IsDirectory)
                    image = new BitmapImage(new Uri(FolderIcon));
            }
            catch (Exception e)
            {
                Trace.TraceError(e.Message);
            }

            return image;
        }
    }
}