﻿/*
MIT License

Copyright(c) 2018 Hammond Soares

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using FileSurf.ViewModel;

namespace FileSurf.Model
{
    /// <summary>
    /// Class that contains the information to be displayed in the UI.
    /// </summary>
    public class DisplayedInfo : BaseViewModel
    {
        #region Fields

        /// <summary>
        /// The directory path.
        /// </summary>
        private string _directoryPath;

        /// <summary>
        /// The directory files.
        /// </summary>
        private ObservableCollection<DisplayItemInfo> _files;

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="DisplayedInfo"/> class.
        /// </summary>
        public DisplayedInfo()
        {
            _files = new ObservableCollection<DisplayItemInfo>();
        }

        #endregion Constructor

        #region Properties

        /// <summary>
        /// Gets or sets the directory path.
        /// </summary>
        public string DirectoryPath
        {
            get => _directoryPath;
            set
            {
                if (_directoryPath != value)
                {
                    GetFiles(value);
                }
                _directoryPath = value;
                OnPropertyChanged(nameof(DirectoryPath));
            }
        }

        /// <summary>
        /// Gets or sets the files.
        /// </summary>
        public ObservableCollection<DisplayItemInfo> Files
        {
            get => _files;
            set
            {
                _files = value;
                OnPropertyChanged(nameof(Files));
            }
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Gets the files.
        /// </summary>
        /// <param name="path">The path.</param>
        private void GetFiles(string path)
        {
            Files.Clear();

            if (!Directory.Exists(path))
                return;

            try
            {
                // Creates collection of file info.
                var fileInfoCollection = new List<DisplayItemInfo>();
                // Adds subdirectories information of every directory found into the collection.
                Directory.GetDirectories(path).ToList().ForEach(filename => fileInfoCollection.Add(new DisplayItemInfo(new FileInfo(filename))));
                // Adds file information of every file found into the collection.
                Directory.GetFiles(path).ToList().ForEach(filename => fileInfoCollection.Add(new DisplayItemInfo(new FileInfo(filename))));
                // Adds the collection to the property.
                Files = new ObservableCollection<DisplayItemInfo>(fileInfoCollection);
            }
            catch (System.Exception ex)
            {
                Trace.TraceError(ex.ToString());
            }
        }

        #endregion Methods
    }
}