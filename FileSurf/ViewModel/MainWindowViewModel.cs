﻿/*
MIT License

Copyright(c) 2018 Hammond Soares

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

using FileSurf.Commands;
using FileSurf.Model;
using System;
using System.IO;
using System.Windows.Forms;
using System.Windows.Input;

namespace FileSurf.ViewModel
{
    public class MainWindowViewModel : BaseViewModel
    {
        private FileInfo _selectedFileLeft;
        private FileInfo _selectedFileRight;

        private DisplayedInfo _displayLeft;
        private DisplayedInfo _displayRight;
        private string _folderIcon;

        public ICommand SelectDirectoryLeftCommand { get; set; }
        public ICommand SelectDirectoryRightCommand { get; set; }
        public ICommand CloseWindowCommand { get; set; }
        public ICommand CopyFileCommand { get; set; }
        public ICommand MoveFileCommand { get; set; }
        public ICommand DeleteFileCommand { get; set; }
        public ICommand ViewFileContentCommand { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindowViewModel" /> class.
        /// </summary>
        public MainWindowViewModel()
        {
            _displayLeft = new DisplayedInfo();
            _displayRight = new DisplayedInfo();

            DisplayLeft.DirectoryPath = @"C:\";
            DisplayRight.DirectoryPath = @"C:\Temp";

            SelectDirectoryLeftCommand = new RelayCommand(OpenFileLeft);
            SelectDirectoryRightCommand = new RelayCommand(OpenFileRight);
            CloseWindowCommand = new RelayCommand(CloseApp);

            CopyFileCommand = new RelayCommand(CopyFile, (o) => SelectedFileLeft != null && !string.IsNullOrEmpty(DisplayRight.DirectoryPath));
            MoveFileCommand = new RelayCommand(MoveFile);
            DeleteFileCommand = new RelayCommand(DeleteFile, (o) => SelectedFileLeft != null);
            ViewFileContentCommand = new RelayCommand(ViewFileContentFromLeftSide, (o) => SelectedFileLeft != null);
        }

        private void ViewFileContentFromLeftSide()
        {
            if (SelectedFileLeft != null)
            {
                var viewer = new View.FileViewer();
                ((FileViewerViewModel)viewer.DataContext).FilePath = SelectedFileLeft.FullName;
                viewer.ShowDialog();
            }
        }

        private void DeleteFile()
        {
        }

        private void MoveFile()
        {
        }

        private void CopyFile()
        {
        }

        private void OpenFileLeft()
        {
            SelectDirectory(DisplayLeft);
        }

        private void OpenFileRight()
        {
            SelectDirectory(DisplayRight);
        }

        private void CloseApp()
        {
            Environment.Exit(0);
        }

        private void SelectDirectory(DisplayedInfo info)
        {
            var dialog = new FolderBrowserDialog();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                info.DirectoryPath = dialog.SelectedPath;
            }
        }

        public DisplayedInfo DisplayLeft
        {
            get => _displayLeft;
            set
            {
                _displayLeft = value;
                OnPropertyChanged(nameof(DisplayLeft));
            }
        }

        public DisplayedInfo DisplayRight
        {
            get => _displayRight;
            set
            {
                _displayRight = value;
                OnPropertyChanged(nameof(DisplayRight));
            }
        }

        public FileInfo SelectedFileLeft
        {
            get => _selectedFileLeft;
            set
            {
                _selectedFileLeft = value;
                OnPropertyChanged(nameof(SelectedFileLeft));
            }
        }

        public FileInfo SelectedFileRight
        {
            get => _selectedFileRight;
            set
            {
                _selectedFileRight = value;
                OnPropertyChanged(nameof(SelectedFileRight));
            }
        }

        public string FolderIcon
        {
            get { return _folderIcon; }
            set
            {
                _folderIcon = value; 
                OnPropertyChanged(nameof(FolderIcon));
            }
        }
    }
}